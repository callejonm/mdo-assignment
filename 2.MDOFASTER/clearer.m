function clearer
close all;
clc;
clearvars;
clear all;
%evalin lets you clear the workspace with an external function
evalin('base','clear variables')
end