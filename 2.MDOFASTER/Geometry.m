function parameters = Geometry(x, constants)


Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = constants.CSTroot;
CSTtip        = constants.CSTtip;
ThetaK        = x(6) ;
ThetaT        = x(7) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;


yk = constants.yk;

xkink = constants.yk * tand(LambdaLEin);
ykink = constants.yk;
zkink = constants.yk*tand(constants.dihedral); % NO DIHEDRAL ANGLE????

xtip = constants.yk * tand(LambdaLEin) + (yt - constants.yk) *tand(LambdaLEout);
ytip = yt;
ztip = yt*tand(constants.dihedral); % NO DIHEDRAL ANGLE????

Ck = Cr - constants.yk*(tand(LambdaLEin)-tand(constants.LambdaTE));

landai = Ck/Cr;
landao = Ct/Ck;

Sw = yk * (Cr + Ck) + (yt-yk)*(Ck + Ct);
MAC = yk*(Cr+Ck)/Sw*2/3*Cr*(landai^2+landai+1)/(landai+1)+...
    (yt-yk)*Cr*landai*(1+landao)/Sw*2/3*Cr*landai*(landao^2+landao+1)/(landao+1);

CSTkink = (yk.*CSTroot+(yt-yk).*CSTtip)./yt;


parameters.xkink = xkink;
parameters.ykink = ykink;
parameters.zkink = zkink;

parameters.xtip = xtip;
parameters.ytip = ytip;
parameters.ztip = ztip;

parameters.Ck = Ck ;

parameters.Sw = Sw;
parameters.MAC = MAC;

parameters.CSTkink = CSTkink;
end
