function [f,vararg] = Optimizer(x, x0,  constants)
global couplings
x = x.*x0;
parameters = Geometry(x, constants);

Wwhat = couplings.Ww;
Wfhat = couplings.Wf;
%fprintf('Entering MDA \n')
%tic
[Lmax,Mmax,Ww, L, D, Wf, counter, Wwhat, Wfhat] = MDA(x,Wwhat,Wfhat , constants, parameters);
%toc
    f = MTOW(Wf, Ww)/217000;


    
    vararg = {Lmax,Mmax,Ww, L, D, Wf, counter, Wwhat, Wfhat};

    couplings.Ww = Ww;
    couplings.Wf = Wf;
   couplings.counter = couplings.counter + 1; 
   plotplanform(x, parameters, constants)
end