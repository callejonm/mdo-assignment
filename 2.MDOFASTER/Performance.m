function  Wf= Performance(        L, D,     Ww, constants)



We_Ws=exp((constants.R*constants.C_T*D)/(constants.V*L));
Wf=(1-0.938*We_Ws.^(-1))/(0.928*We_Ws^(-1))*(9.802997216534909e+04 + Ww);

end

