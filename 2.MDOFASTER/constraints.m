function [cin,ceq] = constraints(x, x0,  constants)
global couplings
x = x.*x0;
Ww = couplings.Ww;
Wf = couplings.Wf;
parameters = Geometry(x, constants);
Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = constants.CSTroot;
CSTtip        = constants.CSTtip;
ThetaK        = x(6) ;
ThetaT        = x(7) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;



Aroot =  Atank(xfrontspar, xbackspar, CSTroot, Cr);
Akink =  Atank(xfrontspar, xbackspar, parameters.CSTkink, parameters.Ck);
Atip  =  Atank(xfrontspar, xbackspar, CSTtip, Ct);

Vtank = 2*0.5*(Aroot + Akink)*(constants.yk - constants.ftank_start) +...
    2*0.5*(Akink + Atip)*(yt*constants.ftank_end - constants.yk) ;
Vfuel = Wf / constants.rho_fuel;

c1 = Vfuel / Vtank / constants.ftank -1 ;

WS = MTOW(Wf, Ww) / parameters.Sw;
WS_ref = constants.WSmax;
c2 = WS/WS_ref - 1;


cin = [c1 c2];
ceq = [];
    function S= Atank(xfrontspar, xbackspar, CST, chord)
        X = linspace(0, 1, 100);
       [Xtu,Xtl,C,Thu,Thl,Cm] =D_airfoil2(CST(1:6)', CST(7:12)', X');
       pos1 = round(100*xbackspar); pos2 = round(100*xfrontspar);
       hsparfront = Xtu(pos1, 2) - Xtl(pos1, 2);
       hsparback = Xtu(pos2, 2) - Xtl(pos2, 2);
       S = (hsparfront + hsparback)*.5*chord.^2 *(xbackspar - xfrontspar);
    end
end