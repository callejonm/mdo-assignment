function f = MTOW(Wf, Ww)
f = 8.878887216534908e+04 + Wf + Ww;
end