function [Lmax,Mmax,Ww, CL, CD, Wf, counter, Wwhat, Wfhat] = MDA(x,Wwhat,Wfhat , constants, parameters)

tol=1e-3; error = 15;
counter = 0;

Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = constants.CSTroot;
CSTtip        = constants.CSTtip;
ThetaK        = x(6) ;
ThetaT        = x(7) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;

Ww=5.804990000000000e+04;
Wf=9.072012783465092e+04;


while abs(Ww-Wwhat)/Ww>tol ||  abs(Wf-Wfhat)/Wf>tol
    if (counter > 0)
        Wwhat = Ww;
        Wfhat = Wf;        
    end

    [Lmax,Mmax]  = Loads       (x,            Wfhat, Wwhat, constants, parameters);
    Ww           = Structures  (x, Lmax,Mmax, Wfhat, Wwhat, constants, parameters);
    [CL, CDWing]       = Aerodynamics(x,            Wfhat, Ww, constants, parameters);
    CD = CDWing + 0.010982732361428;
    Wf           = Performance(        CL, CD,     Ww, constants);



    counter= counter+1;
end