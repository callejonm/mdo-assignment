function [CL,CD]  = Aerodynamics   (  x,            Wf, Ww, constants, parameters)



Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = constants.CSTroot;
CSTtip        = constants.CSTtip;
ThetaK        = x(6) ;
ThetaT        = x(7) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;


%                x    y     z   chord(m)    twist angle (deg) 
AC.Wing.Geom = [0     0     0     Cr        constants.ThetaI;
                parameters.xkink parameters.ykink parameters.zkink parameters.Ck  ThetaK;
                parameters.xtip  parameters.ytip  parameters.ztip  Ct             ThetaT];

% Wing incidence angle (degree)
AC.Wing.inc  = constants.ThetaI;  
            
            
% Airfoil coefficients input matrix
%                    | ->     upper curve coeff.                <-|   | ->       lower curve coeff.       <-| 
AC.Wing.Airfoils   = [CSTroot'; 
                     parameters.CSTkink';
                     CSTtip' ];

AC.Wing.eta = [0;constants.yk/yt;1];  % Spanwise location of the airfoil sections

% Viscous vs inviscid
AC.Visc  = 1;              % 0 for inviscid and 1 for viscous analysis
AC.Aero.MaxIterIndex = 150;    %Maximum number of Iteration for the
                                %convergence of viscous calculation
                                
                                

WTOMAX = MTOW(Wf, Ww);

W = (WTOMAX.*(WTOMAX-Wf)).^(.5);

                                
% Flight Condition
AC.Aero.V     = constants.V;                                 % flight speed (m/s)
AC.Aero.rho   = constants.rho_cruise;                        % air density  (kg/m3) %%According to 1976 ISA
AC.Aero.alt   = constants.h_cruise;                          % flight altitude (m)
AC.Aero.Re    = constants.rho_cruise*parameters.MAC*constants.V / constants.mu_cruise; %reynolds number (bqased on mean aerodynamic chord)
AC.Aero.M     = constants.V / constants.a_cruise;            % flight Mach number 

 AC.Aero.q     = (0.5* AC.Aero.rho*AC.Aero.V.^2);
AC.Aero.CL    = W*9.81/ AC.Aero.q /parameters.Sw;                       % lift coefficient - comment this line to run the code for given alpha%
%AC.Aero.Alpha = 2;                                          % angle of attack -  comment this line to run the code for given cl 

%% 
%tic

Res = Q3D_solver(AC);

%toc

L = parameters.MAC .*  Res.CLwing .* AC.Aero.q;
D = parameters.MAC .*  Res.CDwing .* AC.Aero.q;
CL = Res.CLwing;
CD = Res.CDwing;
end
