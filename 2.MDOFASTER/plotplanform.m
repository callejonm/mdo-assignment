 function plotplanform(x, parameters, constants)

Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = constants.CSTroot;
CSTtip        = constants.CSTtip;
ThetaK        = x(6) ;
ThetaT        = x(7) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;


xkink = parameters.xkink ;
ykink = parameters.ykink ;
zkink = parameters.zkink ;

xtip = parameters.xtip ;
ytip = parameters.ytip ;
ztip = parameters.ztip ;

Ck = parameters.Ck  ;

Sw = parameters.Sw ;
MAC = parameters.MAC ;

CSTkink = parameters.CSTkink ;

X = [0 0;
     xkink, ykink
     xtip, ytip];


X(6,:)  = X(1,:) + [ Cr 0];
X(5,:)  = X(2,:) + [ Ck 0];
X(4,:)  = X(3,:) + [ Ct 0];
figure(1)
plot(X(:,1), X(:,2));
hold on
end 