function [y1,y2,counter, y2hat] = MDA(x1, z1, z2, y2hat)
y2 = 1e9; tol=1e-15; error = 15;
counter = 0;
while abs(y2-y2hat)/y2>tol
    if (counter > 0)
        y2hat = y2;
    end

    y1 = Discipline1(x1, z1, z2, y2hat);
    y2 = Discipline2(x1, z1, z2, y1);
    

    counter= counter+1;
end