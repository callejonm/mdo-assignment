function f = obj(x1, z2, y1, y2)
f = x1^2+z2+y1+exp(-y2);
end