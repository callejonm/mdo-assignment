%Matlab MDF Gauss Seidel implementation of the Sellar problem (Sellar et
%al. 1996)

%Initial values:
x1 = 1;
z1 = 5;
z2 = 2;

%bounds
lb = [0,-10,0];
ub = [10,10,10];

x0 = [x1,z1,z2];
    
%Initial guess for output of discipline 2
    y2_i = 0;
    %call MDA coordinator to compute initial values of coupling variables,
    %because matlab first computes the constraint function
    [y1,y2,counter,y2_c] = MDA(x1,z1,z2,y2_i);

c1 = 3.16 - y1;
c2 = y2 - 24;

% Options for the optimization
options.Display         = 'iter-detailed';
options.Algorithm       = 'sqp';
options.FunValCheck     = 'off';
options.DiffMinChange   = 1e-6;         % Minimum change while gradient searching
options.DiffMaxChange   = 5e-2;         % Maximum change while gradient searching
options.TolCon          = 1e-6;         % Maximum difference between two subsequent constraint vectors [c and ceq]
options.TolFun          = 1e-6;         % Maximum difference between two subsequent objective value
options.TolX            = 1e-6;         % Maximum difference between two subsequent design vectors

options.MaxIter         = 30;           % Maximum iterations

global couplings;
couplings.y1 = y1;
couplings.y2 = y2;
tic;
[x,FVAL,EXITFLAG,OUTPUT] = fmincon(@(x) Optimizer(x),x0,[],[],[],[],lb,ub,@(x) constraints(x),options);
toc;
%[f,vararg] = Optim_MDFGauss(x);
