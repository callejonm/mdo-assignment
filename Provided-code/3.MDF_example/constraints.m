function [cin,ceq] = constraints(x)
global couplings;
y1 = couplings.y1;
y2 = couplings.y2;

c1 = 3.16 - y1;
c2 = y2 - 24;

cin = [c1 c2];
ceq = [];

end