function Error = obj(x)   

%Determine upper and lower CST parameters from design-vector
 Au = x(1:length(x)/2);
 Al = x(length(x)/2+1:length(x));

% Define the Airfoil coordinates
% Read-in the Airfoil coordinate file
fid = fopen('airfoils/withcomb135.dat', 'r');
Coor = fscanf(fid, '%g %g', [2 Inf]) ;
fclose(fid) ;

IndexLowerpart = find(Coor(1, :) == 0,1,  'first');

CoorUpper = Coor(:, 1:IndexLowerpart)';

[CoorUpper(:,1), I] = sort(CoorUpper(:,1));
CoorUpper(:,2) = CoorUpper(I,2);

CoorLower = Coor(:, (IndexLowerpart+1):end)';


[Xtu_1,trash,C_1,Thu_1,Thl_1,Cm_1] = D_airfoil2(Au,Al,CoorUpper(:, 1));
[trash,Xtl_2,C_2,Thu_2,Thl_2,Cm_2] = D_airfoil2(Au,Al,CoorLower(:, 1));

error_up  =( CoorUpper(:, 2) - Xtu_1(:, 2) );
error_low =( CoorLower(:, 2) - Xtl_2(:, 2) );

Error = sum(error_up.^2) + sum(error_low.^2);




