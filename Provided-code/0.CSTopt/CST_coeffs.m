%script to test function D_airfoil2

function  [CSTroot, CSTtip] = CST_coeffs(plot)

if nargin == 0
    plot=0
end


fid = fopen('airfoils/withcomb135.dat', 'r');
Coor = fscanf(fid, '%g %g', [2 Inf]) ;
fclose(fid) ;

IndexLowerpart = find(Coor(1, :) == 0,1,  'first');

CoorUpper = Coor(:, 1:IndexLowerpart)';

[CoorUpper(:,1), I] = sort(CoorUpper(:,1));
CoorUpper(:,2) = CoorUpper(I,2);

CoorLower = Coor(:, (IndexLowerpart+1):end)';


%Au = [1 1.5 4 0.1 0.1];         %upper-surface Bernstein coefficients
%Al = [-1 -1 -1 0.1 0.1];    %lower surface Bernstein coefficients


% This script shows the implementation of the CST airfoil-fitting
% optimization of MDO Tutorial 1

M = 12  %Number of CST-coefficients in design-vector x

%Define optimization parameters
x0 = 1*ones(M,1);     %initial value of design vector x(starting vector for search process)
lb = -1*ones(M,1);    %upper bound vector of x
ub = 1*ones(M,1);     %lower bound vector of x
options=optimset('Display','Iter' ,'TolFun', 1e-9);

tic
[x,fval,exitflag] = fmincon(@obj,x0,[],[],[],[],lb,ub,[],options);

t = toc
%% Ploting the result

M_break=M/2;
X_vect = linspace(0,1,99)';      %points for evaluation along x-axis
Aupp_vect=x(1:M_break);
Alow_vect=x(1+M_break:end);
[Xtu,Xtl,C,Thu,Thl,Cm] = D_airfoil2(Aupp_vect,Alow_vect,X_vect);
if plot ==1
    figure
    hold on
    plot(Xtu(:,1),Xtu(:,2),'b');    %plot upper surface coords
    plot(Xtl(:,1),Xtl(:,2),'b');    %plot lower surface coords
    % plot(X_vect,C,'r');                  %plot class function
    axis([0,1,-0.5,0.5]);



    hold on 
    plot(CoorUpper(:,1),CoorUpper(:,2),'r');    %plot upper surface coords
    plot(CoorLower(:,1),CoorLower(:,2),'r');    %plot lower surface coords
end
%% 14% and 8% thickness ratio at the tip and at the root
t = Xtu(:,2)-Xtl(:,2);

tmax = max(t);


t_tip  = 0.14;
t_root = 0.08;

Aupp_tip = t_tip/tmax*Aupp_vect;
Alow_tip = t_tip/tmax*Alow_vect;
%'The initial Bernstein coefficients for the tip chord are:'
CSTtip =  [Aupp_tip; Alow_tip]

Aupp_root = t_root/tmax*Aupp_vect;
Alow_root = t_root/tmax*Alow_vect;

%'The initial Bernstein coefficients for the root chord are:'
CSTroot = [Aupp_root; Alow_root]


%% Just checking
if plot ==1 

[Xtu,Xtl,C,Thu,Thl,Cm] = D_airfoil2(Aupp_root,Alow_root,X_vect);
[Xtu2,Xtl2,C,Thu,Thl,Cm] = D_airfoil2(Aupp_tip,Alow_tip,X_vect);

figure
hold on
plot(Xtu(:,1),Xtu(:,2),'b');    %plot upper surface coords
plot(Xtl(:,1),Xtl(:,2),'b');    %plot lower surface coords
% plot(X_vect,C,'r');                  %plot class function
axis([0,1,-0.2,0.2]);

figure
hold on
plot(Xtu2(:,1),Xtu2(:,2),'b');    %plot upper surface coords
plot(Xtl2(:,1),Xtl2(:,2),'b');    %plot lower surface coords
% plot(X_vect,C,'r');                  %plot class function
axis([0,1,-0.2,0.2]);
end
end 
