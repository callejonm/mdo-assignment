%% Aerodynamic solver setting
clear all
close all
clc

% Wing planform geometry 
%                x    y     z   chord(m)    twist angle (deg) 
AC.Wing.Geom = [0     0     0     3.5         0;
                0.9  14.5   0     1.4         0];

% Wing incidence angle (degree)
AC.Wing.inc  = 0;   
            
            
% Airfoil coefficients input matrix
%                    | ->     upper curve coeff.                <-|   | ->       lower curve coeff.       <-| 
AC.Wing.Airfoils   = [0.2171    0.3450    0.2975    0.2685    0.2893  -0.1299   -0.2388   -0.1635   -0.0476    0.0797;
                      0.2171    0.3450    0.2975    0.2685    0.2893  -0.1299   -0.2388   -0.1635   -0.0476    0.0797];

AC.Wing.eta = [0;1];  % Spanwise location of the airfoil sections

% Viscous vs inviscid
AC.Visc  = 0;              % 0 for inviscid and 1 for viscous analysis
AC.Aero.MaxIterIndex = 150;    %Maximum number of Iteration for the
                                %convergence of viscous calculation


wing_span = 28;
c_root = 3.5;
tap_ratio = 0.25;
c_tap = tap_ratio*c_root;
Sw = .5*(c_root+c_tap)*wing_span;
                                
% Flight Condition
AC.Aero.V     = 68;            % flight speed (m/s)
AC.Aero.rho   = 1.225;         % air density  (kg/m3)
AC.Aero.alt   = 0;             % flight altitude (m)
AC.Aero.Re    = 1.14;        % reynolds number (bqased on mean aerodynamic chord)
AC.Aero.M     = 0.2;           % flight Mach number 

%AC.Aero.q     = (0.5* AC.Aero.rho*AC.Aero.V^2)
%C.Aero.CL    = n_max*MTOW*9.81/ AC.Aero.q /Sw;          % lift coefficient - comment this line to run the code for given alpha%
AC.Aero.Alpha = 2;             % angle of attack -  comment this line to run the code for given cl 


%% 
tic

Res = Q3D_solver(AC);

toc

%%
Coords = AC.Wing.Geom(:,1:3)
Coords(3,:) = Coords(1,:) + AC.Wing.Geom(1,4)*[1,0,0]
Coords(4,:) = Coords(2,:) + AC.Wing.Geom(2,4)*[1,0,0]


figure
plot(Res.Wing.Yst/wing_span, Res.Wing.ccl)
figure
plot(Res.Wing.Yst/wing_span, Res.Wing.cm_c4)

AC.Aero.q*Res.Wing.ccl
AC.Aero.q*Res.Wing.cm_c4*Sw*0.25
