function [f,vararg] = Optimizer(x, x0,  constants)
global couplings
x = x.*x0;
parameters = Geometry(x, constants);

Wwhat = couplings.Ww;
Wfhat = couplings.Wf;
%fprintf('Entering MDA \n')
%tic
    fileID = fopen('output.txt','a');
    fprintf(fileID, '\n \n ---------------------------------------------------\n');
    fprintf(fileID, 'Function evaluation number %f \n',couplings.counter );
    fprintf(fileID, 'Cr %f Ct %f sweeps %f %f span %f twists %f %f \n', x(1),x(2),x(3),x(4),x(5),x(30),x(31));
    fprintf(fileID, 'CST root upper %f %f %f %f %f %f \n', x(6),x(7),x(8),x(9),x(10),x(11));
    fprintf(fileID, 'CST root lower %f %f %f %f %f %f \n', x(12),x(13),x(14),x(15),x(16),x(17));
    fprintf(fileID, 'CST tip upper %f %f %f %f %f %f \n', x(18),x(19),x(20),x(21),x(22),x(23));
    fprintf(fileID, 'CST tip lower %f %f %f %f %f %f \n \n', x(24),x(25),x(26),x(27),x(28),x(29));
    fclose(fileID);

[Lmax,Mmax,Ww, L, D, Wf, counter, Wwhat, Wfhat] = MDA(x,Wwhat,Wfhat , constants, parameters);
%toc
    f = MTOW(Wf, Ww)/217000;


    
    vararg = {Lmax,Mmax,Ww, L, D, Wf, counter, Wwhat, Wfhat};
    if not(isnan(Ww)) && not(isnan(Wf))
         couplings.Ww = Ww;
         couplings.Wf = Wf;
    end
    

    fileID = fopen('output.txt','a');
    fprintf(fileID, 'MDA converged in %f evaluations' , counter);
    fprintf(fileID, 'Ww %f Wf %f \n \n Lmax', Ww,Wf);
    fprintf(fileID, ' %f ', Lmax);
    fprintf(fileID, '\n Mmax');
    fprintf(fileID, ' %f ', Mmax);
    fprintf(fileID, '\n ');
    fprintf(fileID, 'Lift-to-drag ratio %f \n', L/D);
fclose(fileID);

   couplings.counter = couplings.counter + 1;
%   plotplanform(x, parameters, constants)
end