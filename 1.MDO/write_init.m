%%%_____Routine to write the input file for the EMWET procedure________% %%
function write_init(x,MTOW, Wf, constants, parameters)



Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = x(6:17) ;
CSTtip        = x(18:29) ;
ThetaK        = x(30) ;
ThetaT        = x(31) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;


namefile    =    char('A330-300');
%MTOW        =   Ww+Wf+W_aw;         %[kg];
MZF         =    MTOW-Wf;         %[kg]
nz_max      =    constants.nmax   ;
b        =    2*yt;            %[m]

ftank_start =    constants.ftank_start;
ftank_end   =    constants.ftank_end;
eng_num     =    constants.eng_num;
eng_ypos    =    constants.eng_pos;
eng_mass    =    constants.eng_mass;      
E_al        =    constants.Eal;       
rho_al      =    constants.rho_al;        
Ft_al       =    constants.sigma_yield_tens;       
Fc_al       =    constants.sigma_yield_comp;   
pitch_rib   =    constants.theta_rib;       
eff_factor  =    0.96;                      %Depend on the stringer type

write_airfoil(CSTroot,            'root')
write_airfoil(parameters.CSTkink, 'kink')
write_airfoil(CSTtip,             'tip')

Airfoil1    =    'root';
Airfoil2    =    'kink';
Airfoil3    =    'tip';
section_num =    3;
airfoil_num =    3;

fid = fopen( strcat(namefile,'.init'),'wt');
fprintf(fid, '%g %g \n',MTOW,MZF);
fprintf(fid, '%g \n',nz_max);

fprintf(fid, '%g %g %g %g \n',parameters.Sw,b,section_num,airfoil_num);

fprintf(fid, '0 %s \n',Airfoil1);
fprintf(fid, '%g %s \n',constants.yk/yt,Airfoil2);
fprintf(fid, '1 %s \n',Airfoil3);

fprintf(fid, '%g %g %g %g %g %g \n',...
    Cr,0,0,0,xfrontspar,xbackspar);
fprintf(fid, '%g %g %g %g %g %g \n',...
    parameters.Ck, parameters.xkink,parameters.ykink,parameters.zkink,xfrontspar,xbackspar);
fprintf(fid, '%g %g %g %g %g %g \n',...
    Ct, parameters.xtip, parameters.ytip, parameters.ztip ,xfrontspar,xbackspar);

fprintf(fid, '%g %g \n',ftank_start,ftank_end);

fprintf(fid, '%g \n', eng_num);
fprintf(fid, '%g  %g \n', eng_ypos,eng_mass);

fprintf(fid, '%g %g %g %g \n',E_al,rho_al,Ft_al,Fc_al);
fprintf(fid, '%g %g %g %g \n',E_al,rho_al,Ft_al,Fc_al);
fprintf(fid, '%g %g %g %g \n',E_al,rho_al,Ft_al,Fc_al);
fprintf(fid, '%g %g %g %g \n',E_al,rho_al,Ft_al,Fc_al);

fprintf(fid,'%g %g \n',eff_factor,pitch_rib);
fprintf(fid,'0 \n');
fclose(fid);



function write_airfoil(CST, name)
X = linspace(0,1,99)';
[Xu,Xl,~,~,~,~]=D_airfoil2(CST(1:6),CST(7:12),X);
fid = fopen(strcat(name,'.dat'), 'w');
for i=1:length(Xu)
 fprintf(fid, '%d %d \n',X(i), Xu(i, 2));
end
for i=1:length(Xu)
 fprintf(fid, '%d %d \n',X(i), Xl(i, 2));
end
fclose(fid);

end
end


