 function plotplanform(x, parameters, constants, name)

 if ~exist('name','var')
     % third parameter does not exist, so default it to something
      name = '';
 end
 
Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = x(6:17) ;
CSTtip        = x(18:29) ;
ThetaK        = x(30) ;
ThetaT        = x(31) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;


xkink = parameters.xkink ;
ykink = parameters.ykink ;
zkink = parameters.zkink ;

xtip = parameters.xtip ;
ytip = parameters.ytip ;
ztip = parameters.ztip ;

Ck = parameters.Ck  ;

Sw = parameters.Sw ;
MAC = parameters.MAC ;

CSTkink = parameters.CSTkink ;

X = [0 0;
     xkink, ykink
     xtip, ytip];


X(6,:)  = X(1,:) + [ Cr 0];
X(5,:)  = X(2,:) + [ Ck 0];
X(4,:)  = X(3,:) + [ Ct 0];

if sum(double(name)) == sum(double('Optimized'))
    tagColor = 'k';
elseif sum(double(name)) ==  sum(double('Initial'))
    tagColor = 'b';
end

figure(1)
if sum(double(name ))== double('')
    plot(X(:,1), X(:,2),'HandleVisibility','off');
else
    plot(X(:,1), X(:,2), tagColor, 'DisplayName' , strcat(name, ' planform'));
end
xlabel('x [m]', 'Interpreter', 'latex')
ylabel('y/y{t} [m]', 'Interpreter', 'latex')
title('Overlapped plot of the initial and optimized wing planform', 'Interpreter', 'latex')
hold on
plotairfoil(CSTroot, 2, name, tagColor)
xlabel('x/c', 'Interpreter', 'latex')
ylabel('z/c', 'Interpreter', 'latex')
title('Overlapped view of the initial and optimized root airfoil', 'Interpreter', 'latex')

plotairfoil(CSTtip, 3, name, tagColor)

xlabel('x/c', 'Interpreter', 'latex')
ylabel('z/c', 'Interpreter', 'latex')
title('Overlapped view of the initial and optimized tip airfoil', 'Interpreter', 'latex')

function plotairfoil(CST, number, name, tagColor)
M_break = length(CST)./2;
X_vect = linspace(0,1,99)';      %points for evaluation along x-axis
Aupp_vect=CST(1:M_break);
Alow_vect=CST(1+M_break:end);
[Xtu,Xtl,C,Thu,Thl,Cm] = D_airfoil2(Aupp_vect,Alow_vect,X_vect);
figure(number)

hold on
if sum(double(name ))== sum(double(''))
plot(Xtu(:,1),Xtu(:,2),'HandleVisibility','off');    %plot upper surface coords
plot(Xtl(:,1),Xtl(:,2),'HandleVisibility','off');    %plot lower surface coords
else  
plot(Xtu(:,1),Xtu(:,2),tagColor,   'DisplayName' , strcat(name, ' airfoil'));
plot(Xtl(:,1),Xtl(:,2),tagColor,  'HandleVisibility','off');

end
axis([0,1,-0.15, .15]);


end 
 end