clearer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Define Initial values and constant parameters through the optimization:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%global x0 constants parameters
Cr = 12.386937;
Ct = Cr * 0.2510;
LambdaLEin = 36.1801331;
LambdaLEout = 36.1801331;
yt = 29.0;

%[CSTroot, CSTtip] = CST_coeffs(1)
CSTroot=[0.243188679253764;0.0832240395872928;0.278333557657806;0.0935212464767531;0.289263504317772;0.397196057718086;-0.234548211305095;-0.170352825617254;-0.0483422561204444;-0.497335357172826;0.0770830026676601;0.338520591400361];
CSTtip =[0.138964959573579;0.0475565940498816;0.159047747233032;0.0534407122724304;0.165293431038727;0.226969175838906;-0.134027549317197;-0.0973444717812882;-0.0276241463545397;-0.284191632670186;0.0440474300958057;0.193440337943063];
ThetaK = 2;
ThetaT = -1;
xfrontspar = 0.175;
xbackspar = 0.575;

x0 = [Cr; Ct; LambdaLEin; LambdaLEout; yt; CSTroot; CSTtip; ThetaK;ThetaT];

x0hat = ones(size(x0));

constants.xfrontspar = xfrontspar;
constants.xbackspar = xbackspar;

% Constants of the flight. 
constants.LambdaTEin=0;
constants.yk=9.165361;
constants.ThetaI=4.5;
constants.R=8334000;
constants.h_cruise=10056;
constants.V=242;
constants.VMAX=257;
constants.rho_cruise = 0.409727;
constants.a_cruise = 299.208;
constants.mu_cruise = 0.0000146672;
constants.Gamma=5;
constants.xr=0;
constants.yr=0;
constants.zr=0;
constants.ftank=0.93;
constants.rho_fuel=817.15;
constants.ftank_start=0.1;
constants.ftank_end=0.85;
constants.eng_num=1;
constants.eng_pos=0.246;
constants.eng_mass=5851.3;
constants.sigma_yield_tens=295e6;
constants.sigma_yield_comp=295e6;
constants.Eal=70000e6;
constants.rho_al=2800;
constants.theta_rib=0.5;
constants.C_T = 1.8639e-4;
constants.dihedral = 0;
constants.LambdaTE = 5;
constants.nmax = 2.5;
constants.WSmax = 597.63;

%Define geometric parameters (function at the end of the file
parameters = Geometry (x0hat.*x0, constants);

%Bounds [Cr; Ct  ; sweeps   ; span ; CST Coefficients                 ; Twist (kink) ;root]  
lb = [ .8; .6  ; .85;  .9;  0.8; .8*ones(size(CSTroot)); .8*ones(size(CSTtip)); .5 ; 0];
ub = [1.2; 1.6  ; 1.2; 1.2; 1.05 ;1.2*ones(size(CSTroot)); 1.1*ones(size(CSTtip)); 1.5; 2];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OPTIMIZATION LOOP: FIRST CALL THE MDA FOR INITIAL VALUES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

W_AW = 8.878887216534908e+04;

Wwhat = 47491;
Wfhat = 8.072012783465092e+04; 
tic
% Inside MDA: dimensional values of x
[Lmax,Mmax,Ww, CL, CD, Wf, counter, Wwhat, Wfhat] = MDA(x0,Wwhat,Wfhat , constants, parameters);
toc
%% THEN CONSTRAINTS AND OPTIMIZER

% Options for the optimization
options.Display         = 'iter-detailed';
options.Algorithm       = 'sqp';
options.FunValCheck     = 'off';
options.DiffMinChange   = 1e-4;         % Minimum change while gradient searching
options.DiffMaxChange   = 1e-2;         % Maximum change while gradient searching
options.TolCon          = 1e-6;         % Maximum difference between two subsequent constraint vectors [c and ceq]
options.TolFun          = 1e-6;         % Maximum difference between two subsequent objective value
options.TolX            = 1e-6;         % Maximum difference between two subsequent design vectors
options.UseParallel     = false;

options.MaxIter         = 1000;           % Maximum iterations
options.PlotFcns = {@optimplotfval, @optimplotx, @optimplotfirstorderopt, ...
                    @optimplotfunccount, @optimplotconstrviolation, @optimplotstepsize };

global couplings;
couplings.Wf = Wf;
couplings.Ww = Ww;
couplings.counter = 0;

% Inside Optimizer & constraints: adimensional values of x
tic;
[x,FVAL,EXITFLAG,OUTPUT] = fmincon(...
    @(x) Optimizer(x, x0,constants),...
    x0hat,[],[],[],[],lb,ub,...
    @(x) constraints(x, x0, constants) ,options)
toc;
[f,vararg] = Optimizer(x, x0,  constants);

%% FINAL PLOTS 


%x = [14.864226 3.001271 31.981001 32.246875 26.100000 ...
% 0.231409 0.087244 0.285760 0.093713 0.310436 0.428120 ...
% -0.251115 -0.173896 -0.049803 -0.500601 0.076970 0.360236... 
% 0.139127 0.048718 0.167891 0.055700 0.170282 0.231335 ...
%-0.138359 -0.100131 -0.027575 -0.290150 0.045035 0.196743  2.102967 -1.058327 ]';
% 
x = [ 1.1224;
1.1139;
0.9914;
0.9000;
0.8000;
1.0067;
0.9278;
1.0813;
1.0463;
1.0047;
1.1603;
1.1486;
0.8946;
0.9553;
1.0910;
0.8002;
1.0924;
1.0229;
1.0892;
1.0941;
0.9275;
1.0993;
1.0189;
0.8691;
1.0829;
1.0145;
0.8747;
0.9472;
0.8502;
1.0565;
0.9937];
% close all
couplings.Ww = 1.9926e+04; Ww = couplings.Ww;
couplings.Wf = 90986.544472 ; Wf = couplings.Wf;
x = x.*x0;
parameters = Geometry(x, constants);
% close 1 2 3 
plotplanform(x0, Geometry(x0, constants), constants, 'Initial')
plotplanform(x, parameters, constants, 'Optimized')

figure(1)
legend('-DynamicLegend', 'Location', 'northwest', 'Interpreter', 'latex');
figure(2)
legend('-DynamicLegend', 'Location', 'southeast', 'Interpreter', 'latex');
figure(3)
legend('-DynamicLegend', 'Location', 'southeast', 'Interpreter', 'latex');


[CL,CD] = Aero_draw   (  x,            Wf, Ww, constants, parameters);
[CL,CD] = Aero_draw0   (  x0,            8.3688e+04, 1.3148e+04, constants, Geometry(x0, constants));

[Lmax,Mmax]  = Loads_draw       (x,            Wf, Ww, constants, parameters);
[Lmax,Mmax]  = Loads_draw0       (x0,            8.3688e+04, 1.3148e+04, constants, Geometry(x0, constants));
figure(4)
legend('-DynamicLegend', 'Location', 'northeast', 'Interpreter', 'latex');
figure(5)
legend('-DynamicLegend', 'Location', 'northeast', 'Interpreter', 'latex');
figure(6)
legend('-DynamicLegend', 'Location', 'northeast', 'Interpreter', 'latex');
figure(7)
legend('-DynamicLegend', 'Location', 'northeast', 'Interpreter', 'latex');
figure(8)
legend('-DynamicLegend', 'Location', 'northeast', 'Interpreter', 'latex');

convergplots()
for k=1:10
 h=figure(k);
 saveas(h,sprintf('plots/fig%d.png',k)); 
end



