function [CL,CD]  = Aero_draw   (  x,            Wf, Ww, constants, parameters)



Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = x(6:17) ;
CSTtip        = x(18:29) ;
ThetaK        = x(30) ;
ThetaT        = x(31) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;


%                x    y     z   chord(m)    twist angle (deg) 
AC.Wing.Geom = [0     0     0     Cr        constants.ThetaI;
                parameters.xkink parameters.ykink parameters.zkink parameters.Ck  ThetaK;
                parameters.xtip  parameters.ytip  parameters.ztip  Ct             ThetaT];

% Wing incidence angle (degree)
AC.Wing.inc  = constants.ThetaI;  
            
            
% Airfoil coefficients input matrix
%                    | ->     upper curve coeff.                <-|   | ->       lower curve coeff.       <-| 
AC.Wing.Airfoils   = [CSTroot'; 
                     parameters.CSTkink';
                     CSTtip' ];

AC.Wing.eta = [0;constants.yk/yt;1];  % Spanwise location of the airfoil sections

% Viscous vs inviscid
AC.Visc  = 1;              % 0 for inviscid and 1 for viscous analysis
AC.Aero.MaxIterIndex = 300;    %Maximum number of Iteration for the
                                %convergence of viscous calculation
                                
                                

WTOMAX = MTOW(Wf, Ww);

W = (WTOMAX.*(WTOMAX-Wf)).^(.5);

                                
% Flight Condition
AC.Aero.V     = constants.V;                                 % flight speed (m/s)
AC.Aero.rho   = constants.rho_cruise;                        % air density  (kg/m3) %%According to 1976 ISA
AC.Aero.alt   = constants.h_cruise;                          % flight altitude (m)
AC.Aero.Re    = constants.rho_cruise*parameters.MAC*constants.V / constants.mu_cruise; %reynolds number (bqased on mean aerodynamic chord)
AC.Aero.M     = constants.V / constants.a_cruise;            % flight Mach number 

 AC.Aero.q     = (0.5* AC.Aero.rho*AC.Aero.V.^2);
AC.Aero.CL    = W*9.81/ AC.Aero.q /parameters.Sw;                       % lift coefficient - comment this line to run the code for given alpha%
%AC.Aero.Alpha = 2;                                          % angle of attack -  comment this line to run the code for given cl 

%% 
%tic

Res = Q3D_solver(AC);

%toc

L = parameters.MAC .*  Res.CLwing .* AC.Aero.q;
D = parameters.MAC .*  Res.CDwing .* AC.Aero.q;
CL = Res.CLwing;
CD = Res.CDwing;

%% Plots and stuff
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');

figure(4)
plot(Res.Wing.Yst/yt, Res.Wing.ccl, 'k-*',  'DisplayName','Optimized design')
hold on
title('Spanwise lift distribution at the design point', 'Interpreter', 'latex')
xlabel('$y/y_{t}$', 'Interpreter', 'latex')
ylabel('$C \times Cl $', 'Interpreter', 'latex')


figure(5)
hold on
plot(Res.Wing.Yst/yt, Res.Wing.cl, 'k-*', 'DisplayName', 'Optimized design')

title('Lift coefficient distribution at the design point', 'Interpreter', 'latex')
xlabel('$y/y_{t}$', 'Interpreter', 'latex')
ylabel('Cl', 'Interpreter', 'latex')


figure(6)
hold on
Cdtot = interp1( Res.Section.Y, Res.Section.Cd, Res.Wing.Yst);  

plot(Res.Wing.Yst/yt, Cdtot + Res.Wing.cdi,  'k-*',  'DisplayName','$Cd$ optimized')
plot(Res.Wing.Yst/yt, Res.Wing.cdi, 'b-*', 'DisplayName', '$Cd_{i}$ optimized' )
plot(Res.Wing.Yst/yt, Cdtot, 'r-*',  'DisplayName','$Cd_{0}$ optimized')

title('Drag coefficient vs span position at the design point', 'Interpreter', 'latex')
xlabel('$y/y_{t}$', 'Interpreter', 'latex')
ylabel('Cd', 'Interpreter', 'latex')


end
