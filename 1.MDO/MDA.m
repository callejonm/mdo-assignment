function [Lmax,Mmax,Ww, CL, CD, Wf, counter, Wwhat, Wfhat] = MDA(x,Wwhat,Wfhat , constants, parameters)

tol=1e-3; errorF = 15;errorW = 15;
counter = 0;

Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = x(6:17) ;
CSTtip        = x(18:29) ;
ThetaK        = x(30) ;
ThetaT        = x(31) ;
xfrontspar    = constants.xfrontspar;
xbackspar     = constants.xbackspar;

Ww=5.804990000000000e+04;
Wf=9.072012783465092e+04;


while (errorW>tol || errorF >tol) && counter <30
    if counter==29
        fprintf('MDA did not converge. errors: %f for wing, %f for fuel', errorW, errorF);
    end
    if (counter > 0)
        Wwhat = Ww;
        Wfhat = Wf;        
    end

    [Lmax,Mmax]  = Loads       (x,            Wfhat, Wwhat, constants, parameters);
    
   if  any(isnan(Lmax)) || any(isnan(Mmax))
       fprintf('MDA got an error. Returning NaN to the optimizer.'); Ww = NaN; CL = NaN; CD = NaN; Wf = NaN;
       break
   end
    Ww           = Structures  (x, Lmax,Mmax, Wfhat, Wwhat, constants, parameters);
   if  isnan(Ww) 
       fprintf('MDA got an error. Returning NaN to the optimizer.');CL = NaN; CD = NaN; Wf = NaN;
       break
   end
    [CL, CDWing]       = Aerodynamics(x,            Wfhat, Ww, constants, parameters);

    CD = CDWing + 0.010982732361428*363.2829/parameters.Sw;
    Wf           = Performance(        CL, CD,     Ww, constants);

   if   isnan(CL) ||isnan(CD) || isnan(Wf) 
       fprintf('MDA got an error. Returning NaN to the optimizer.');
       break
   end
    
    errorW = abs(Ww-Wwhat)/Ww;
    errorF = abs(Wf-Wfhat)/Wf;
    counter= counter+1;
end