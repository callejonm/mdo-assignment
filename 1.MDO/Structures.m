function Ww = Structures (x, Lmax,Mmax, Wf, Ww, constants, parameters)
W = MTOW(Wf, Ww);

write_init(x,W, Wf, constants, parameters);
write_loads(Lmax, Mmax);

EMWET A330-300


aux = importdata('A330-300.weight');

Ww = aux.data;
end

