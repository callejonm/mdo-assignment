function  Wf= Performance(        L, D,     MTOW);

global x0
global constants
We_Ws=exp((constants.R*constants.C_T*D)/(constants.V*L));
Wf=(1-0.938*We_Ws.^(-1))*MTOW;

end

