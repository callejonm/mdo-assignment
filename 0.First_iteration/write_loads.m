function write_loads(Lmax, Mmax)
X = linspace(0,1,length(Lmax));

namefile    =    char('A330-300');
fid = fopen( strcat(namefile,'.load'),'wt');

for i=1:length(Lmax)
fprintf(fid, '%g %g %g \n',X(i), Lmax(i), Mmax(i));
end
fclose(fid)

end