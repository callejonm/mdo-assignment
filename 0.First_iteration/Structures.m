function Ww = Structures(x,Lmax,Mmax,MTOW, Wf)


write_init(x,MTOW, Wf)
write_loads(Lmax, Mmax)

EMWET A330-300


aux = importdata('A330-300.weight');

Ww = aux.data;
end

