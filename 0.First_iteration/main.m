clearer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Define Initial values and constant parameters through the optimization:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global x0 constants parameters
Cr = 12.386937;
Ct = Cr * 0.2510;
LambdaLEin = 36.1801331;
LambdaLEout = 36.1801331;
yt = 29.0;

%[CSTroot, CSTtip] = CST_coeffs(1)
CSTroot=[0.243188679253764;0.0832240395872928;0.278333557657806;0.0935212464767531;0.289263504317772;0.397196057718086;-0.234548211305095;-0.170352825617254;-0.0483422561204444;-0.497335357172826;0.0770830026676601;0.338520591400361];
CSTtip =[0.138964959573579;0.0475565940498816;0.159047747233032;0.0534407122724304;0.165293431038727;0.226969175838906;-0.134027549317197;-0.0973444717812882;-0.0276241463545397;-0.284191632670186;0.0440474300958057;0.193440337943063];
ThetaK = 2;
ThetaT = 5;
xfrontspar = 0.175;
xbackspar = 0.575;

x0 = [Cr; Ct; LambdaLEin; LambdaLEout; yt; CSTroot; CSTtip; ThetaK;ThetaT; xfrontspar; xbackspar];

x0hat = ones(size(x0));

x = x0.*x0hat ;

% Constants of the flight. 
constants.LambdaTEin=0;
constants.yk=9.165361;
constants.ThetaI=4.5;
constants.R=8334000;
constants.h_cruise=10056;
constants.V=242;
constants.VMAX=257;
constants.rho_cruise = 0.409727;
constants.a_cruise = 299.208;
constants.mu_cruise = 0.0000146672;
constants.Gamma=5;
constants.xr=0;
constants.yr=0;
constants.zr=0;
constants.ftank=0.93;
constants.rho_fuel=817.15;
constants.ftank_start=0.1;
constants.ftank_end=0.85;
constants.eng_num=1;
constants.eng_pos=0.246;
constants.eng_mass=5851.3;
constants.sigma_yield_tens=295e6;
constants.sigma_yield_comp=295e6;
constants.Eal=70000e6;
constants.rho_al=2800;
constants.theta_rib=0.5;
constants.C_T = 1.8639e-4;
constants.dihedral = 0;
constants.LambdaTE = 5;
constants.nmax = 2.5;

%Define geometric parameters (function at the end of the file
parameters = Geometry (x);

%Bounds 
lb = [.8; .6; .7; .7; .8; .8*ones(size(CSTroot)); .8*ones(size(CSTtip)); 0;0; .8; .87];
ub = [1.2; 1.6; 1.2; 1.2; 1.2; 1.2*ones(size(CSTroot)); 1.2*ones(size(CSTtip)); 1.4;1.4; 1.2; 1.2];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIRST ITERATION: WE CALCULATE W_AW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MTOW = 217000;
[Lmax,Mmax]  = Loads       (x,        MTOW);
Wf           = Performance (16,1,     MTOW)
[CL, CD_Wing] =  Aerodynamics   (x, MTOW, Wf);
CD_AW = (CL - 16 * CD_Wing) /16 ; 

Ww = Structures(x,Lmax,Mmax,MTOW, Wf);

display('The aircraft minus wing weight is: ')
W_AW = MTOW - Ww - Wf

function parameters = Geometry(x)
Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = x(6:17) ;
CSTtip        = x(18:29) ;
ThetaK        = x(30) ;
ThetaT        = x(31) ;
xfrontspar    = x(32) ;
xbackspar     = x(33) ;

global constants;

yk = constants.yk;

xkink = constants.yk * tand(LambdaLEin);
ykink = constants.yk;
zkink = constants.yk*tand(constants.dihedral); % NO DIHEDRAL ANGLE????

xtip = constants.yk * tand(LambdaLEin) + (yt - constants.yk) *tand(LambdaLEout);
ytip = yt;
ztip = yt*tand(constants.dihedral); % NO DIHEDRAL ANGLE????

Ck = Cr - constants.yk*(tand(LambdaLEin)-tand(constants.LambdaTE));

landai = Ck/Cr;
landao = Ct/Ck;

Sw = yk * (Cr + Ck) + (yt-yk)*(Ck + Ct);
MAC = yk*(Cr+Ck)/Sw*2/3*Cr*(landai^2+landai+1)/(landai+1)+...
    (yt-yk)*Cr*landai*(1+landao)/Sw*2/3*Cr*landai*(landao^2+landao+1)/(landao+1);

CSTkink = (yk.*CSTroot+(yt-yk).*CSTtip)./yt;


parameters.xkink = xkink;
parameters.ykink = ykink;
parameters.zkink = zkink;

parameters.xtip = xtip;
parameters.ytip = ytip;
parameters.ztip = ztip;

parameters.Ck = Ck ;

parameters.Sw = Sw;
parameters.MAC = MAC;

parameters.CSTkink = CSTkink;
end

