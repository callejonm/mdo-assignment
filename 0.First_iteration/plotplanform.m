 function plotplanform(yk, lambdaLEroot, yt, lambdaLEkink, cr, ct)
X = [0 0;
yk*tand(lambdaLEroot), yk
yk*tand(lambdaLEroot) + (yt-yk)*tand(lambdaLEkink), yt];


X(6,:) = X(1,:) + [ cr 0];
X(5,:) = X(2,:) + [ cr-yk*tand(lambdaLEroot) 0];
X(4,:)  = X(3,:) + [ ct 0];
figure
 plot(X(:,1), X(:,2));
 
end 