function [Lmax,Mmax,Ww, L, D, Wf, counter, Wwhat, Wfhat] = MDA(x,Wwhat,Wfhat )
y2 = 1e9; tol=1e-15; error = 15;
counter = 0;
global x0

x = x.* x0 ;

Cr            = x(1) ;
Ct            = x(2) ;
LambdaLEin    = x(3) ;
LambdaLEout   = x(4) ;
yt            = x(5) ;
CSTroot       = x(6:17) ;
CSTtip        = x(18:29) ;
ThetaK        = x(30) ;
ThetaT        = x(31) ;
xfrontspar    = x(32) ;
xbackspar     = x(33) ;



while abs(Ww-Wwhat)/Ww>tol ||  abs(Wf-Wfhat)/Wf>tol
    if (counter > 0)
        Wwhat = Ww;
        Wfhat = Wf;        
    end
    
    [Lmax,Mmax]  = Loads       (x,            Wf, Ww);
    Ww           = Structures  (x, Lmax,Mmax, Wf, Ww) ;
    [L, D]       = Aerodynamics(x,            Wf, Ww);
    Wf           = Performance (        L, D,     Ww);

    

    counter= counter+1;
end